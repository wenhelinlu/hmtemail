﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1022
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMTERPClient.Resources {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("HMTERPClient.Resources.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   使用此强类型资源类，为所有资源查找
        ///   重写当前线程的 CurrentUICulture 属性。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        public static System.Drawing.Bitmap _lock {
            get {
                object obj = ResourceManager.GetObject("_lock", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap arrow {
            get {
                object obj = ResourceManager.GetObject("arrow", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap arrow_right {
            get {
                object obj = ResourceManager.GetObject("arrow_right", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap btnconfirmbg {
            get {
                object obj = ResourceManager.GetObject("btnconfirmbg", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap btnconfirmbg_hover {
            get {
                object obj = ResourceManager.GetObject("btnconfirmbg_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap buyerinfosetting {
            get {
                object obj = ResourceManager.GetObject("buyerinfosetting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap close {
            get {
                object obj = ResourceManager.GetObject("close", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap close_hover {
            get {
                object obj = ResourceManager.GetObject("close_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap commodityinfosetting {
            get {
                object obj = ResourceManager.GetObject("commodityinfosetting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap companyinfosetting {
            get {
                object obj = ResourceManager.GetObject("companyinfosetting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap currentmonth {
            get {
                object obj = ResourceManager.GetObject("currentmonth", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap dbpathsetting {
            get {
                object obj = ResourceManager.GetObject("dbpathsetting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap downloadquery1 {
            get {
                object obj = ResourceManager.GetObject("downloadquery1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap downloadquery1_hover {
            get {
                object obj = ResourceManager.GetObject("downloadquery1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap enable {
            get {
                object obj = ResourceManager.GetObject("enable", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap errortag {
            get {
                object obj = ResourceManager.GetObject("errortag", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap exceltransfer1 {
            get {
                object obj = ResourceManager.GetObject("exceltransfer1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap exceltransfer1_hover {
            get {
                object obj = ResourceManager.GetObject("exceltransfer1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_add {
            get {
                object obj = ResourceManager.GetObject("icon_add", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_close {
            get {
                object obj = ResourceManager.GetObject("icon_close", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_close_vat {
            get {
                object obj = ResourceManager.GetObject("icon_close_vat", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_dbpathsetting {
            get {
                object obj = ResourceManager.GetObject("icon_dbpathsetting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_delete {
            get {
                object obj = ResourceManager.GetObject("icon_delete", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_listdisplayoption {
            get {
                object obj = ResourceManager.GetObject("icon_listdisplayoption", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_reminder {
            get {
                object obj = ResourceManager.GetObject("icon_reminder", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_shop {
            get {
                object obj = ResourceManager.GetObject("icon_shop", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_speed {
            get {
                object obj = ResourceManager.GetObject("icon_speed", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_system_close {
            get {
                object obj = ResourceManager.GetObject("icon_system_close", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_system_line {
            get {
                object obj = ResourceManager.GetObject("icon_system_line", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_system_max {
            get {
                object obj = ResourceManager.GetObject("icon_system_max", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_system_mini {
            get {
                object obj = ResourceManager.GetObject("icon_system_mini", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_system_setting {
            get {
                object obj = ResourceManager.GetObject("icon_system_setting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_systemregister {
            get {
                object obj = ResourceManager.GetObject("icon_systemregister", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_table {
            get {
                object obj = ResourceManager.GetObject("icon_table", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_target {
            get {
                object obj = ResourceManager.GetObject("icon_target", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap icon_verify {
            get {
                object obj = ResourceManager.GetObject("icon_verify", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap IMPORT_B {
            get {
                object obj = ResourceManager.GetObject("IMPORT_B", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap import1 {
            get {
                object obj = ResourceManager.GetObject("import1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap import2 {
            get {
                object obj = ResourceManager.GetObject("import2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap INPUT_B {
            get {
                object obj = ResourceManager.GetObject("INPUT_B", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap invoicedconfirm1 {
            get {
                object obj = ResourceManager.GetObject("invoicedconfirm1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap invoicedconfirm1_hover {
            get {
                object obj = ResourceManager.GetObject("invoicedconfirm1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap invoiceexportheader {
            get {
                object obj = ResourceManager.GetObject("invoiceexportheader", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap kpzstitle {
            get {
                object obj = ResourceManager.GetObject("kpzstitle", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_btn {
            get {
                object obj = ResourceManager.GetObject("login_btn", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_btn_hover {
            get {
                object obj = ResourceManager.GetObject("login_btn_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_input_name {
            get {
                object obj = ResourceManager.GetObject("login_input_name", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_input_name_hover {
            get {
                object obj = ResourceManager.GetObject("login_input_name_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_input_password {
            get {
                object obj = ResourceManager.GetObject("login_input_password", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_input_password_hover {
            get {
                object obj = ResourceManager.GetObject("login_input_password_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_vat {
            get {
                object obj = ResourceManager.GetObject("login_vat", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap login_vat_skynj {
            get {
                object obj = ResourceManager.GetObject("login_vat_skynj", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap loginbg {
            get {
                object obj = ResourceManager.GetObject("loginbg", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap loginbgnew {
            get {
                object obj = ResourceManager.GetObject("loginbgnew", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap loginname {
            get {
                object obj = ResourceManager.GetObject("loginname", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap loginpassword {
            get {
                object obj = ResourceManager.GetObject("loginpassword", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap logintitle {
            get {
                object obj = ResourceManager.GetObject("logintitle", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap logintitlename {
            get {
                object obj = ResourceManager.GetObject("logintitlename", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap logotitlenamenew {
            get {
                object obj = ResourceManager.GetObject("logotitlenamenew", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap main_bg {
            get {
                object obj = ResourceManager.GetObject("main_bg", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap mainlogo {
            get {
                object obj = ResourceManager.GetObject("mainlogo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap map_pin {
            get {
                object obj = ResourceManager.GetObject("map_pin", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap minimise {
            get {
                object obj = ResourceManager.GetObject("minimise", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap mx_sq {
            get {
                object obj = ResourceManager.GetObject("mx_sq", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap mx_zk {
            get {
                object obj = ResourceManager.GetObject("mx_zk", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_ {
            get {
                object obj = ResourceManager.GetObject("nav_", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_active {
            get {
                object obj = ResourceManager.GetObject("nav_active", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_annrowline {
            get {
                object obj = ResourceManager.GetObject("nav_annrowline", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon {
            get {
                object obj = ResourceManager.GetObject("nav_icon", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon1_hover {
            get {
                object obj = ResourceManager.GetObject("nav_icon1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon2 {
            get {
                object obj = ResourceManager.GetObject("nav_icon2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon2_hover {
            get {
                object obj = ResourceManager.GetObject("nav_icon2_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon3 {
            get {
                object obj = ResourceManager.GetObject("nav_icon3", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon3_hover {
            get {
                object obj = ResourceManager.GetObject("nav_icon3_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon4 {
            get {
                object obj = ResourceManager.GetObject("nav_icon4", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon4_hover {
            get {
                object obj = ResourceManager.GetObject("nav_icon4_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon5 {
            get {
                object obj = ResourceManager.GetObject("nav_icon5", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_icon5_hover {
            get {
                object obj = ResourceManager.GetObject("nav_icon5_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap nav_line {
            get {
                object obj = ResourceManager.GetObject("nav_line", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap navispliter {
            get {
                object obj = ResourceManager.GetObject("navispliter", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap newerrortag16 {
            get {
                object obj = ResourceManager.GetObject("newerrortag16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap newrighttag16 {
            get {
                object obj = ResourceManager.GetObject("newrighttag16", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap pagewelcomenew {
            get {
                object obj = ResourceManager.GetObject("pagewelcomenew", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap person {
            get {
                object obj = ResourceManager.GetObject("person", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap query {
            get {
                object obj = ResourceManager.GetObject("query", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap righttag {
            get {
                object obj = ResourceManager.GetObject("righttag", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap righttag_gou {
            get {
                object obj = ResourceManager.GetObject("righttag_gou", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap sbsjdcnew {
            get {
                object obj = ResourceManager.GetObject("sbsjdcnew", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap sbsjdcnew_hover {
            get {
                object obj = ResourceManager.GetObject("sbsjdcnew_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap sbsjdcnew1 {
            get {
                object obj = ResourceManager.GetObject("sbsjdcnew1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap sbsjdcnew1_hover {
            get {
                object obj = ResourceManager.GetObject("sbsjdcnew1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap setting {
            get {
                object obj = ResourceManager.GetObject("setting", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap shrink_sq {
            get {
                object obj = ResourceManager.GetObject("shrink_sq", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap shrink_zk {
            get {
                object obj = ResourceManager.GetObject("shrink_zk", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap speed_bg {
            get {
                object obj = ResourceManager.GetObject("speed_bg", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap submit {
            get {
                object obj = ResourceManager.GetObject("submit", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap submit_hover {
            get {
                object obj = ResourceManager.GetObject("submit_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap system_icon_close {
            get {
                object obj = ResourceManager.GetObject("system_icon_close", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap system_icon_mini {
            get {
                object obj = ResourceManager.GetObject("system_icon_mini", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap system_icon_mini_hover {
            get {
                object obj = ResourceManager.GetObject("system_icon_mini_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap system_icon_more {
            get {
                object obj = ResourceManager.GetObject("system_icon_more", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap system_icon_more_hover {
            get {
                object obj = ResourceManager.GetObject("system_icon_more_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap systemregister {
            get {
                object obj = ResourceManager.GetObject("systemregister", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap table_head {
            get {
                object obj = ResourceManager.GetObject("table_head", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap table_head1 {
            get {
                object obj = ResourceManager.GetObject("table_head1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap table_head2 {
            get {
                object obj = ResourceManager.GetObject("table_head2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap table_head3 {
            get {
                object obj = ResourceManager.GetObject("table_head3", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap toolbarspliter {
            get {
                object obj = ResourceManager.GetObject("toolbarspliter", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap uninvoicewaring1_hover {
            get {
                object obj = ResourceManager.GetObject("uninvoicewaring1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap uninvoicewarning1 {
            get {
                object obj = ResourceManager.GetObject("uninvoicewarning1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap Validate_B {
            get {
                object obj = ResourceManager.GetObject("Validate_B", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap validate1 {
            get {
                object obj = ResourceManager.GetObject("validate1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap validate2 {
            get {
                object obj = ResourceManager.GetObject("validate2", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap vatexport1 {
            get {
                object obj = ResourceManager.GetObject("vatexport1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap vatexport1_hover {
            get {
                object obj = ResourceManager.GetObject("vatexport1_hover", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap welcome {
            get {
                object obj = ResourceManager.GetObject("welcome", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap window_close {
            get {
                object obj = ResourceManager.GetObject("window_close", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap wrench {
            get {
                object obj = ResourceManager.GetObject("wrench", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        public static System.Drawing.Bitmap wrongtag_cha {
            get {
                object obj = ResourceManager.GetObject("wrongtag_cha", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
