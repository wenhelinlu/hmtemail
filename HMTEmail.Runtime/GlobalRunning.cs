﻿using LumiSoft.Net.IMAP.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMTEmail.Runtime
{
    public class GlobalRunning
    {
        /// <summary>
        /// 当前连接的IMAP实例
        /// </summary>
        public static IMAP_Client CurrentIMAPClient { get; set; }

        /// <summary>
        /// 写日志
        /// </summary>
        public static log4net.ILog Logger
        {
            get
            {
                return log4net.LogManager.GetLogger("Logger");
            }
        }
    }
}