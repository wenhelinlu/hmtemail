﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    public class Int2ImageExportHZBConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());

            string path = "";

            switch (v)
            {
                case 2: //表示当前所属期状态
                    path = "/HMTERPClient.Resources;component/Resources/eportorange.png";
                    break;

                default: //其他
                    path = "/HMTERPClient.Resources;component/Resources/exportblue.png";
                    break;
            }
            return path;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}