﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 当前所属期状态时显示认证管理按钮
    /// </summary>
    public class Int2VisibilityAuthButtonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());
            Visibility status = Visibility.Collapsed;
            switch (v)
            {
                case 2: //表示当前所属期状态
                    status = Visibility.Visible;
                    break;

                default: //其他
                    break;
            }
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}