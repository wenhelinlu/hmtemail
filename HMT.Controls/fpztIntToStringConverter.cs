﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    public class fpztIntToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string sReturn = string.Empty;
            switch (System.Convert.ToString(value))
            {
                case "0":
                    sReturn = "正常";
                    break;

                case "1":
                    sReturn = "失控";
                    break;

                case "2":
                    sReturn = "作废";
                    break;

                case "3":
                    sReturn = "红冲";
                    break;

                case "4":
                    sReturn = "异常";
                    break;

                case "5":
                    sReturn = "认证异常";
                    break;

                default:
                    sReturn = System.Convert.ToString(value);
                    break;
            }
            return sReturn;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}