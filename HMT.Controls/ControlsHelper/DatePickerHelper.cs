﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace HMT.Controls.ControlsHelper
{
    public class DatePickerHelper
    {
        #region 日历控件只显示年月

        public static bool GetIsYear(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsYearProperty);
        }

        public static void SetIsYear(DependencyObject obj, bool value)
        {
            obj.SetValue(IsYearProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsYear.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsYearProperty = DependencyProperty.RegisterAttached("IsYear", typeof(bool), typeof(DatePickerHelper), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsMonthYearChanged)));

        public static bool GetIsMonthYear(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonthYearProperty);
        }

        public static void SetIsMonthYear(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonthYearProperty, value);
        }

        public static readonly DependencyProperty IsMonthYearProperty = DependencyProperty.RegisterAttached("IsMonthYear", typeof(bool), typeof(DatePickerHelper), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsMonthYearChanged)));
        // Using a DependencyProperty as the backing store for IsMonthYear.  This enables animation, styling, binding, etc...

        private static void OnIsMonthYearChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var datePicker = (DatePicker)d;
            System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                new Action<DatePicker, DependencyPropertyChangedEventArgs>(SetCalendarEventHandlers), datePicker, e);
        }

        private static void SetCalendarEventHandlers(DatePicker arg1, DependencyPropertyChangedEventArgs arg2)
        {
            if (arg2.NewValue == arg2.OldValue)
            {
                return;
            }

            if ((bool)arg2.NewValue)
            {
                arg1.CalendarOpened += arg1_CalendarOpened;
                arg1.CalendarClosed += arg1_CalendarClosed;
            }
            else
            {
                arg1.CalendarOpened -= arg1_CalendarOpened;
                arg1.CalendarClosed -= arg1_CalendarClosed;
            }
        }

        private static void arg1_CalendarClosed(object sender, RoutedEventArgs e)
        {
            var datePicker = (DatePicker)sender;
            var calendar = GetDatePickerCalendar(sender);
            datePicker.SelectedDate = calendar.SelectedDate;
            calendar.DisplayModeChanged -= calendar_DisplayModeChanged;
        }

        private static void arg1_CalendarOpened(object sender, RoutedEventArgs e)
        {
            var calendar = GetDatePickerCalendar(sender);
            if (GetIsYear(sender as DatePicker))
            {
                calendar.DisplayMode = CalendarMode.Decade;
            }
            else
            {
                calendar.DisplayMode = CalendarMode.Year;
            }
            calendar.DisplayModeChanged += calendar_DisplayModeChanged;
        }

        private static void calendar_DisplayModeChanged(object sender, CalendarModeChangedEventArgs e)
        {
            var calendar = (Calendar)sender;
            var datePicker = GetCalendarsDatePicker(calendar);

            bool mode = (GetIsYear(datePicker) && calendar.DisplayMode != CalendarMode.Year) || (GetIsMonthYear(datePicker) && calendar.DisplayMode != CalendarMode.Month);
            if (mode)
            {
                return;
            }

            calendar.SelectedDate = GetSelectedCalendarDate(calendar.DisplayDate);
            var textBox = datePicker.Template.FindName("PART_TextBox", datePicker) as Control;

            if (textBox != null)
            {
                var wartmarkControl = textBox.Template.FindName("Message", textBox) as ContentControl;
                if (wartmarkControl != null)
                {
                    wartmarkControl.Content = ((DateTime)calendar.SelectedDate).ToString("yyyy-MM");
                }
            }

            datePicker.IsDropDownOpen = false;
        }

        private static Calendar GetDatePickerCalendar(object sender)
        {
            var datePicker = (DatePicker)sender;
            var popup = (Popup)datePicker.Template.FindName("PART_Popup", datePicker);
            return ((Calendar)popup.Child);
        }

        private static DatePicker GetCalendarsDatePicker(FrameworkElement child)
        {
            var parent = (FrameworkElement)child.Parent;
            if (parent.Name == "PART_Root")
            {
                return (DatePicker)parent.TemplatedParent;
            }
            return GetCalendarsDatePicker(parent);
        }

        private static DateTime? GetSelectedCalendarDate(DateTime? selectedDate)
        {
            if (!selectedDate.HasValue)
            {
                return null;
            }
            return new DateTime(selectedDate.Value.Year, selectedDate.Value.Month, 1);
        }

        #endregion 日历控件只显示年月

        public static object GetWatermark(DependencyObject obj)
        {
            return (Object)obj.GetValue(WatermarkProperty);
        }

        public static void SetWatermark(DependencyObject obj, Object value)
        {
            obj.SetValue(WatermarkProperty, value);
        }

        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.RegisterAttached("Watermark", typeof(object), typeof(DatePickerHelper), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnWatermarkChanged)));

        private static void OnWatermarkChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var datePicker = d as DatePicker;
            if (datePicker == null)
                return;
            if (datePicker.IsLoaded)
            {
                SetWatermarkInternal(datePicker, e.NewValue);
            }
            else
            {
                RoutedEventHandler loadedHandler = null;
                loadedHandler = delegate
                {
                    datePicker.Loaded -= loadedHandler;
                    SetWatermarkInternal(datePicker, e.NewValue);
                };
                datePicker.Loaded += loadedHandler;
            }
        }

        private static void SetWatermarkInternal(DatePicker d, Object value)
        {
            var textBox = d.Template.FindName("PART_TextBox", d) as Control;
            if (textBox != null)
            {
                var wartmarkControl = textBox.Template.FindName("PART_Watermark", textBox) as ContentControl;
                if (wartmarkControl != null)
                {
                    wartmarkControl.Content = value;
                }
            }
        }
    }
}