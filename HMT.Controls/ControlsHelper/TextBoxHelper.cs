﻿using System.Windows;
using System.Windows.Controls;

namespace HMT.Controls.ControlsHelper
{
    //文本框的水印附加属性
    public class TextBoxMonitor : DependencyObject
    {
        public static readonly DependencyProperty IsMonitoringProperty =
           DependencyProperty.RegisterAttached("IsMonitoring", typeof(bool), typeof(TextBoxMonitor), new UIPropertyMetadata(false, OnIsMonitoringChanged));

        public static bool GetIsMonitoring(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonitoringProperty);
        }

        public static void SetIsMonitoring(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonitoringProperty, value);
        }

        public static readonly DependencyProperty TextBoxLengthProperty =
DependencyProperty.RegisterAttached("TextBoxLength", typeof(int), typeof(TextBoxMonitor), new UIPropertyMetadata(0));

        public static int GetTextBoxLength(DependencyObject obj)
        {
            return (int)obj.GetValue(TextBoxLengthProperty);
        }

        public static void SetTextBoxLength(DependencyObject obj, int value)
        {
            obj.SetValue(TextBoxLengthProperty, value);
        }

        public static void OnIsMonitoringChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var tx = d as TextBox;
            if (tx == null)
            {
                return;
            }
            if ((bool)e.NewValue)
            {
                tx.TextChanged += new TextChangedEventHandler(tx_TextChanged);
            }
            else
            {
                tx.TextChanged -= new TextChangedEventHandler(tx_TextChanged);
            }
        }

        private static void tx_TextChanged(object sender, TextChangedEventArgs e)
        {
            var tx = sender as TextBox;
            if (tx == null)
            {
                return;
            }
            SetTextBoxLength(tx, tx.Text.Length);
        }
    }

    internal class TextBoxHelper
    {
    }
}