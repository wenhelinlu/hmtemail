﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 已过税款所属期或者当前所属期状态显示导出汇总表按钮
    /// </summary>
    public class Int2VisibilityExportHZBButtonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());
            Visibility status = Visibility.Collapsed;
            switch (v)
            {
                case 1:  //表示已过税款所属期状态
                    status = Visibility.Visible;
                    break;

                case 2: //表示当前所属期状态
                    status = Visibility.Visible;
                    break;
            }
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}