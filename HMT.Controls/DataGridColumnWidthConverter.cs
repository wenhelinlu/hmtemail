﻿using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// double类型和DataGridLength之间的转换器
    /// </summary>
    public class DataGridColumnWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //bool b = System.Convert.ToBoolean(value);
            //return b ? Visibility.Visible : Visibility.Collapsed;
            double d = System.Convert.ToDouble(value);
            DataGridLength width = new DataGridLength(d, DataGridLengthUnitType.Pixel);
            return width;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DataGridLength width = (DataGridLength)value;
            double d = width.DisplayValue;
            return d;
        }
    }
}