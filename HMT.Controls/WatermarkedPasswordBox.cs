﻿using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace HMT.Controls
{
    public class WatermarkedPasswordBox : TextBox
    {
        private bool IsResponseChange = false;

        static WatermarkedPasswordBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WatermarkedPasswordBox), new FrameworkPropertyMetadata(typeof(WatermarkedPasswordBox)));
        }

        public WatermarkedPasswordBox()
        {
            this.Loaded += WatermarkedPasswordBox_Loaded;
        }

        private void WatermarkedPasswordBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsPasswordBox)
            {
                IsResponseChange = false;
                this.Text = ConvertToPasswordChar(PasswordStr.Length);
                IsResponseChange = true;
            }
        }

        public string Watermark
        {
            get { return (string)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Watermark.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register("Watermark", typeof(string), typeof(WatermarkedPasswordBox), new PropertyMetadata(""));

        public CornerRadius BorderCornerRadius
        {
            get { return (CornerRadius)GetValue(BorderCornerRadiusProperty); }
            set { SetValue(BorderCornerRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BorderCornerRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BorderCornerRadiusProperty =
            DependencyProperty.Register("BorderCornerRadius", typeof(CornerRadius), typeof(WatermarkedPasswordBox), null);

        /// <summary>
        /// 是否为密码框
        /// </summary>
        public bool IsPasswordBox
        {
            get { return (bool)GetValue(IsPasswordBoxProperty); }
            set { SetValue(IsPasswordBoxProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPasswordBox.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPasswordBoxProperty =
            DependencyProperty.Register("IsPasswordBox", typeof(bool), typeof(WatermarkedPasswordBox), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsPasswordBoxChanged)));

        /// <summary>
        /// 替换明文的单个密码字符
        /// </summary>

        public char PasswordChar
        {
            get { return (char)GetValue(PasswordCharProperty); }
            set { SetValue(PasswordCharProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PasswordChar.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasswordCharProperty =
            DependencyProperty.Register("PasswordChar", typeof(char), typeof(WatermarkedPasswordBox), new PropertyMetadata('*'));

        /// <summary>
        /// 密码字符串
        /// </summary>
        public string PasswordStr
        {
            get { return (string)GetValue(PasswordStrProperty); }
            set { SetValue(PasswordStrProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PasswordStr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PasswordStrProperty =
            DependencyProperty.Register("PasswordStr", typeof(string), typeof(WatermarkedPasswordBox), new PropertyMetadata(string.Empty));

        private static void OnIsPasswordBoxChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            (sender as WatermarkedPasswordBox).SetEvent();
        }

        private void SetEvent()
        {
            if (IsPasswordBox)
            {
                //IsResponseChange = false;
                PasswordStr = Text;
                this.Text = ConvertToPasswordChar(Text.Length);

                //IsResponseChange = true;
                this.TextChanged += WatermarkedPasswordBox_TextChanged;
            }
            else
            {
                this.TextChanged -= WatermarkedPasswordBox_TextChanged;
                this.Text = PasswordStr;
            }
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            if (!IsPasswordBox)
            {
                PasswordStr = Text;
            }
        }

        private void WatermarkedPasswordBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int lastOffset = 0;
            if (!IsResponseChange)
                return;
            foreach (TextChange c in e.Changes)
            {
                PasswordStr = PasswordStr.Remove(c.Offset, c.RemovedLength);
                PasswordStr = PasswordStr.Insert(c.Offset, Text.Substring(c.Offset, c.AddedLength));
                lastOffset = c.Offset;
            }
            IsResponseChange = false;
            this.Text = ConvertToPasswordChar(Text.Length);
            IsResponseChange = true;
            this.SelectionStart = lastOffset + 1;
        }

        private string ConvertToPasswordChar(int length)
        {
            StringBuilder PasswordBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                PasswordBuilder.Append(PasswordChar);
            }
            return PasswordBuilder.ToString();
        }
    }
}