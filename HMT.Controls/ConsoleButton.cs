﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace HMT.Controls
{
    /// <summary>
    /// 定制的工作台页面月份控制
    /// </summary>
    public class ConsoleButton : Button
    {
        /// <summary>
        /// 导出汇总表按钮背景图片
        /// </summary>
        public ImageBrush ExportHZBButtonBackground
        {
            get { return (ImageBrush)GetValue(ExportHZBButtonBackgroundProperty); }
            set { SetValue(ExportHZBButtonBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExportHZBButtonBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExportHZBButtonBackgroundProperty =
            DependencyProperty.Register("ExportHZBButtonBackground", typeof(ImageBrush), typeof(ConsoleButton), new PropertyMetadata(null));

        /// <summary>
        /// 是否显示认证按钮
        /// </summary>
        public Visibility ShowAuthButton
        {
            get { return (Visibility)GetValue(ShowAuthButtonProperty); }
            set { SetValue(ShowAuthButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowAuthButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowAuthButtonProperty =
            DependencyProperty.Register("ShowAuthButton", typeof(Visibility), typeof(ConsoleButton), new PropertyMetadata(Visibility.Collapsed));

        /// <summary>
        /// 是否显示查看明细按钮
        /// </summary>
        public Visibility ShowDetailButton
        {
            get { return (Visibility)GetValue(ShowDetailButtonProperty); }
            set { SetValue(ShowDetailButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowDetailButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowDetailButtonProperty =
            DependencyProperty.Register("ShowDetailButton", typeof(Visibility), typeof(ConsoleButton), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// 是否显示导出汇总表按钮
        /// </summary>
        public Visibility ShowExportHZBButton
        {
            get { return (Visibility)GetValue(ShowExportHZBButtonProperty); }
            set { SetValue(ShowExportHZBButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowExportHZBButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowExportHZBButtonProperty =
            DependencyProperty.Register("ShowExportHZBButton", typeof(Visibility), typeof(ConsoleButton), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// 是否显示锁定层Border
        /// </summary>
        public Visibility ShowLockBorder
        {
            get { return (Visibility)GetValue(ShowLockBorderProperty); }
            set { SetValue(ShowLockBorderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowLockBorder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowLockBorderProperty =
            DependencyProperty.Register("ShowLockBorder", typeof(Visibility), typeof(ConsoleButton), new PropertyMetadata(Visibility.Collapsed));

        /// <summary>
        /// 发票份数
        /// </summary>
        public string InvoiceNos
        {
            get { return (string)GetValue(InvoiceNosProperty); }
            set { SetValue(InvoiceNosProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InvoiceNos.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InvoiceNosProperty =
            DependencyProperty.Register("InvoiceNos", typeof(string), typeof(ConsoleButton), new PropertyMetadata("---"));

        /// <summary>
        /// 税金合计
        /// </summary>
        public string TaxMoneySummary
        {
            get { return (string)GetValue(TaxMoneySummaryProperty); }
            set { SetValue(TaxMoneySummaryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TaxMoneySummary.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TaxMoneySummaryProperty =
            DependencyProperty.Register("TaxMoneySummary", typeof(string), typeof(ConsoleButton), new PropertyMetadata("---"));

        /// <summary>
        /// 待勾选发票信息
        /// </summary>
        public string WaitForCheck
        {
            get { return (string)GetValue(WaitForCheckProperty); }
            set { SetValue(WaitForCheckProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WaitForCheck.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WaitForCheckProperty =
            DependencyProperty.Register("WaitForCheck", typeof(string), typeof(ConsoleButton), new PropertyMetadata("---"));

        /// <summary>
        /// 已勾选待认证发票信息
        /// </summary>
        public string WaitForAuth
        {
            get { return (string)GetValue(WaitForAuthProperty); }
            set { SetValue(WaitForAuthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WaitForAuth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WaitForAuthProperty =
            DependencyProperty.Register("WaitForAuth", typeof(string), typeof(ConsoleButton), new PropertyMetadata("---"));

        /// <summary>
        /// 已认证发票信息
        /// </summary>
        public string HasAuthed
        {
            get { return (string)GetValue(HasAuthedProperty); }
            set { SetValue(HasAuthedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasAuthed.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasAuthedProperty =
            DependencyProperty.Register("HasAuthed", typeof(string), typeof(ConsoleButton), new PropertyMetadata("---"));

        /// <summary>
        /// 显示bd_normal
        /// </summary>
        public Visibility ShowNormalBorder
        {
            get { return (Visibility)GetValue(ShowNormalBorderProperty); }
            set { SetValue(ShowNormalBorderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowNormalBorder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowNormalBorderProperty =
            DependencyProperty.Register("ShowNormalBorder", typeof(Visibility), typeof(ConsoleButton), new PropertyMetadata(Visibility.Visible));

        /// <summary>
        /// 月份文字显示颜色
        /// </summary>
        public SolidColorBrush MonthColor
        {
            get { return (SolidColorBrush)GetValue(MonthColorProperty); }
            set { SetValue(MonthColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MonthColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MonthColorProperty =
            DependencyProperty.Register("MonthColor", typeof(SolidColorBrush), typeof(ConsoleButton), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));

        /// <summary>
        /// 蓝色蒙版颜色（鼠标hover时，当月份为已认证状态时，显示淡蓝色，其他状态为透明）
        /// </summary>
        public SolidColorBrush BlueBorderColorBrush
        {
            get { return (SolidColorBrush)GetValue(BlueBorderColorBrushProperty); }
            set { SetValue(BlueBorderColorBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BlueBorderColorBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BlueBorderColorBrushProperty =
            DependencyProperty.Register("BlueBorderColorBrush", typeof(SolidColorBrush), typeof(ConsoleButton), new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));
    }
}