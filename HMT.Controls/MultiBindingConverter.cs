﻿using System;
using System.Linq;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 用于Command中传递多个参数时使用
    /// </summary>
    public class MultiBindingConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return values.ToList(); //将多个参数以List的方式传递
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}