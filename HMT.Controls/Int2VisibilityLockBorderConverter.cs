﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 当为未到税款所属期状态时，显示一层蒙版Border
    /// </summary>
    public class Int2VisibilityLockBorderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());
            Visibility status = Visibility.Collapsed;
            switch (v)
            {
                case 3: //表示未到税款所属期状态
                    status = Visibility.Visible;
                    break;

                default: //其他
                    break;
            }
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}