﻿using System;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace HMT.Controls
{
    [ValueConversion(typeof(byte[]), typeof(BitmapImage))]
    public class Bytes2ImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "";
            }
            byte[] img = null;
            if (!string.IsNullOrEmpty(value.ToString()))
            {
                try
                {
                    img = (byte[])value;
                }
                catch
                {
                    return "";
                }
            }
            if (img == null)
            {
                return "";
            }
            return ShowSelectedIMG(img);                //以流的方式显示图片的方法
        }

        //转换器中二进制转化为BitmapImage  datagrid绑定仙石的
        private BitmapImage ShowSelectedIMG(byte[] img)
        {
            try
            {
                if (img != null)
                {
                    //img是从数据库中读取出来的字节数组
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(img);

                    ms.Seek(0, System.IO.SeekOrigin.Begin);
                    BitmapImage newBitmapImage = new BitmapImage();
                    newBitmapImage.BeginInit();
                    newBitmapImage.StreamSource = ms;
                    newBitmapImage.EndInit();
                    return newBitmapImage;

                    ////img是从数据库中读取出来的字节数组
                    //using (var ms = new System.IO.MemoryStream(img))
                    //{
                    //    ms.Seek(0, System.IO.SeekOrigin.Begin);
                    //    BitmapImage newBitmapImage = new BitmapImage();
                    //    newBitmapImage.BeginInit();
                    //    newBitmapImage.StreamSource = ms;
                    //    newBitmapImage.EndInit();
                    //    return newBitmapImage;
                    //}

                    //using (var ms = new System.IO.MemoryStream(img))
                    //{
                    //    var image = new BitmapImage();
                    //    image.BeginInit();
                    //    image.CacheOption = BitmapCacheOption.OnLoad; // here
                    //    image.StreamSource = ms;
                    //    image.EndInit();
                    //    return image;
                    //}
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}