﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace HMT.Controls
{
    /// <summary>
    /// 蓝色蒙版（bd_blue)背景色
    /// </summary>
    public class Int2ColorBlueBorderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());

            //默认为透明
            SolidColorBrush brush = new SolidColorBrush(Colors.Transparent);
            switch (v)
            {
                case 1: //表示当前所属期状态
                    System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml("#48b2ef");
                    brush = new SolidColorBrush(Color.FromArgb(color.A, color.R, color.G, color.B));
                    break;
            }

            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}