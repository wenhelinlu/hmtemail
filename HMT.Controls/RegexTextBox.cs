﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace HMT.Controls
{
    public class RegexTextBox : TextBox
    {
        #region 依赖属性

        public static readonly DependencyProperty XWmkTextProperty; //水印文字
        public static readonly DependencyProperty XWmkForegroundProperty; //水印着色
        public static readonly DependencyProperty XIsErrorProperty; //是否字段有误
        public static readonly DependencyProperty XErrorMessageProperty;//错误提示信息
        public static readonly DependencyProperty XAllowNullProperty;  //是否允许为空
        public static readonly DependencyProperty XRegExpProperty; // 正则表达式

        #endregion 依赖属性

        private RegexTextBoxValidationRule _regexTextBoxValidationRule;

        #region 公开属性

        /// <summary>
        /// 水印文字
        /// </summary>
        public String XWmkText
        {
            get
            {
                return base.GetValue(RegexTextBox.XWmkTextProperty) as String;
            }
            set
            {
                base.SetValue(RegexTextBox.XWmkTextProperty, value);
            }
        }

        /// <summary>
        /// 水印着色
        /// </summary>
        public Brush XWmkForeground
        {
            get
            {
                return base.GetValue(RegexTextBox.XWmkForegroundProperty) as Brush;
            }
            set
            {
                base.SetValue(RegexTextBox.XWmkForegroundProperty, value);
            }
        }

        /// <summary>
        /// 是否字段有误
        /// </summary>
        public bool XIsError
        {
            get
            {
                return (bool)base.GetValue(RegexTextBox.XIsErrorProperty);
            }
            set
            {
                base.SetValue(RegexTextBox.XIsErrorProperty, value);
            }
        }

        /// <summary>
        /// 是否允许为空
        /// </summary>
        public bool XAllowNull
        {
            get
            {
                return (bool)base.GetValue(RegexTextBox.XAllowNullProperty);
            }
            set
            {
                base.SetValue(RegexTextBox.XAllowNullProperty, value);
            }
        }

        /// <summary>
        /// 正则表达式
        /// </summary>
        public string XRegExp
        {
            get
            {
                return base.GetValue(RegexTextBox.XRegExpProperty) as string;
            }
            set
            {
                base.SetValue(RegexTextBox.XRegExpProperty, value);
            }
        }

        /// <summary>
        /// 错误提示信息
        /// </summary>
        public string XErrorMessage
        {
            get
            {
                return base.GetValue(RegexTextBox.XErrorMessageProperty) as string;
            }
            set
            {
                base.SetValue(RegexTextBox.XErrorMessageProperty, value);
            }
        }

        #endregion 公开属性

        #region 内部方法

        /// <summary>
        /// 注册事件
        /// </summary>
        public RegexTextBox()
        {
            this.LostFocus += new RoutedEventHandler(RegexTextBox_LostFocus);
            this.GotFocus += new RoutedEventHandler(RegexTextBox_GotFocus);
            this.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(RegexTextBox_PreviewMouseDown);
        }

        /// <summary>
        /// 静态构造函数
        /// </summary>
        static RegexTextBox()
        {
            //注册依赖属性
            XWmkTextProperty = DependencyProperty.Register("XWmkText", typeof(String), typeof(RegexTextBox), new PropertyMetadata(null));
            XAllowNullProperty = DependencyProperty.Register("XAllowNull", typeof(bool), typeof(RegexTextBox), new PropertyMetadata(true));
            XIsErrorProperty = DependencyProperty.Register("XIsError", typeof(bool), typeof(RegexTextBox), new PropertyMetadata(false));
            XErrorMessageProperty = DependencyProperty.Register("XErrorMessage", typeof(string), typeof(RegexTextBox), new PropertyMetadata(""));
            XRegExpProperty = DependencyProperty.Register("XRegExp", typeof(string), typeof(RegexTextBox), new PropertyMetadata(""));
            XWmkForegroundProperty = DependencyProperty.Register("XWmkForeground", typeof(Brush), typeof(RegexTextBox), new PropertyMetadata(Brushes.Silver));

            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(RegexTextBox), new FrameworkPropertyMetadata(typeof(RegexTextBox)));
        }

        /// <summary>
        /// 应用模板时验证一次默认值
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            ValidateInput(true);
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            ValidateInput(true);
        }

        /// <summary>
        /// 内容更改时再次验证
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            ValidateInput(true);
        }

        /// <summary>
        /// 鼠标点击时选中文字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegexTextBox_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!this.IsFocused)
            {
                TextBox textBox = e.Source as TextBox;
                textBox.Focus();
                e.Handled = true;
            }
        }

        /// <summary>
        /// 获取焦点时选中文字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegexTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.SelectAll();
        }

        /// <summary>
        /// 失去焦点时检查输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegexTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ValidateInput(true);
        }

        /// <summary>
        /// 验证输入
        /// </summary>
        private void ValidateInput(bool isValidate = false)
        {
            this.XIsError = false;
            if (XAllowNull == false && this.Text.Trim() == "")
            {
                this.XIsError = true;
            }
            if (Regex.IsMatch(this.Text.Trim(), XRegExp) == false)
            {
                this.XIsError = true;
            }

            BindingExpression expression = GetBindingExpression(RegexTextBox.TextProperty);
            if (expression == null)
                return;

            Binding binding = expression.ParentBinding;
            if (binding == null)
                return;
            if (_regexTextBoxValidationRule == null)
            {
                _regexTextBoxValidationRule = new RegexTextBoxValidationRule(this.XRegExp, this.XAllowNull, this.XErrorMessage);
                binding.ValidationRules.Add(_regexTextBoxValidationRule);
            }
            if (isValidate)
            {
                expression.UpdateSource();
            }
        }

        #endregion 内部方法
    }
}