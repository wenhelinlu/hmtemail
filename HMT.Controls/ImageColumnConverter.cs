﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    public class ImageColumnConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isInvoice = (bool)value;
            if (isInvoice)
            {
                return "/HMTERPClient.Resources;component/Resources/righttag_gou.png";
            }
            else
            {
                return "/HMTERPClient.Resources;component/Resources/wrongtag_cha.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}