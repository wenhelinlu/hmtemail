﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace HMT.Controls
{
    public class AutoFilteredComboBox : ComboBox
    {
        private int silenceEvents = 0;

        public AutoFilteredComboBox()
        {
            DependencyPropertyDescriptor textProperty = DependencyPropertyDescriptor.FromProperty(ComboBox.TextProperty, typeof(AutoFilteredComboBox));
            textProperty.AddValueChanged(this, this.OnTextChanged);

            this.RegisterIsCaseSensitiveChangeNotification();
            this.IsEditable = true;
        }

        #region IsCaseSensitive Dependency Property

        public static readonly DependencyProperty IsCaseSensitiveProperty =
            DependencyProperty.Register("IsCaseSensitive", typeof(bool), typeof(AutoFilteredComboBox), new UIPropertyMetadata(false));

        [System.ComponentModel.Description("The Way the combobox treats the case sensitivity of typed text.")]
        [System.ComponentModel.Category("AutoFiltered ComboBox")]
        [System.ComponentModel.DefaultValue(true)]
        public bool IsCaseSensitive
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                return (bool)this.GetValue(IsCaseSensitiveProperty);
            }
            [System.Diagnostics.DebuggerStepThrough]
            set
            {
                this.SetValue(IsCaseSensitiveProperty, value);
            }
        }

        protected virtual void OnIsCaseSentitiveChanged(object sender, EventArgs e)
        {
            if (this.IsCaseSensitive)
            {
                this.IsTextSearchEnabled = false;
            }
            this.RefreshFilter();
        }

        private void RegisterIsCaseSensitiveChangeNotification()
        {
            System.ComponentModel.DependencyPropertyDescriptor.FromProperty(IsCaseSensitiveProperty,
                typeof(AutoFilteredComboBox)).AddValueChanged(this, this.OnIsCaseSentitiveChanged);
        }

        #endregion IsCaseSensitive Dependency Property

        #region DropDownOnFocus Dependency Property

        public static readonly DependencyProperty DropDownOnFocusProperty =
            DependencyProperty.Register("DropDownOnFocus", typeof(bool), typeof(AutoFilteredComboBox), new UIPropertyMetadata(true));

        [System.ComponentModel.Description("The Way the combobox behaves when it receives focus.")]
        [System.ComponentModel.Category("AutoFiltered ComboBox")]
        [System.ComponentModel.DefaultValue(true)]
        public bool DropDownOnFocus
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                return (bool)this.GetValue(DropDownOnFocusProperty);
            }
            [System.Diagnostics.DebuggerStepThrough]
            set
            {
                this.SetValue(DropDownOnFocusProperty, value);
            }
        }

        #endregion DropDownOnFocus Dependency Property

        #region Handle selection

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.EditableTextBox.SelectionChanged += new RoutedEventHandler(EditableTextBox_SelectionChanged);
        }

        private int start = 0, length = 0;

        private void EditableTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (this.silenceEvents == 0)
            {
                this.start = ((TextBox)(e.OriginalSource)).SelectionStart;
                this.length = ((TextBox)(e.OriginalSource)).SelectionLength;

                this.RefreshFilter();
            }
        }

        protected TextBox EditableTextBox
        {
            get
            {
                TextBox txtBox = (TextBox)this.Template.FindName("PART_EditableTextBox", this);
                return txtBox;
            }
        }

        #endregion Handle selection

        #region Handle focus

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            if (this.ItemsSource != null && this.DropDownOnFocus)
            {
                this.IsDropDownOpen = true;
            }
        }

        #endregion Handle focus

        #region Handle filtering

        private void RefreshFilter()
        {
            if (this.ItemsSource != null)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(this.ItemsSource);
                view.Refresh();
                this.IsDropDownOpen = true;
            }
        }

        private bool FilterPredicate(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (this.Text.Length == 0)
            {
                return true;
            }

            string prefix = this.Text;
            if (this.length > 0 && this.start + this.length == this.Text.Length)
            {
                prefix = prefix.Substring(0, this.start);
            }
            return value.ToString().Contains(prefix);
        }

        #endregion Handle filtering

        protected override void OnItemsSourceChanged(System.Collections.IEnumerable oldValue, System.Collections.IEnumerable newValue)
        {
            if (newValue != null)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(newValue);
                view.Filter += this.FilterPredicate;
            }

            if (oldValue != null)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(oldValue);
                view.Filter -= this.FilterPredicate;
            }
            base.OnItemsSourceChanged(oldValue, newValue);
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            if (!this.IsTextSearchEnabled && this.silenceEvents == 0)
            {
                this.RefreshFilter();

                if (this.Text.Length > 0)
                {
                    foreach (object item in CollectionViewSource.GetDefaultView(this.ItemsSource))
                    {
                        int text = item.ToString().Length, prefix = this.Text.Length;
                        this.SelectedItem = item;

                        this.silenceEvents++;
                        this.EditableTextBox.Text = item.ToString();
                        this.EditableTextBox.Select(prefix, text - prefix);
                        this.silenceEvents--;
                        break;
                    }
                }
            }
        }
    }
}