﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 已过税款所属期或未到税款所属期状态时显示NormalBorder
    /// </summary>
    public class Int2VisibilityNormalBorderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());
            Visibility status = Visibility.Collapsed;
            switch (v)
            {
                case 1:  //表示已过税款所属期状态
                    status = Visibility.Visible;
                    break;

                case 3: //表示未到税款所属期状态
                    status = Visibility.Visible;
                    break;
            }
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}