﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HMT.Controls
{
    /// <summary>
    /// 数据加载动画
    /// </summary>
    /// <remarks>
    /// 创建日期：2014年9月23日
    /// 创建者：孙永昆
    /// 版权：南京擎天科技有限公司
    /// 修改人：
    /// 修改日期：
    /// </remarks>
    public partial class CircularProgressBar : INotifyPropertyChanged
    {
        #region Data

        private readonly DispatcherTimer _animationTimer;

        #endregion Data

        #region Constructor

        public CircularProgressBar()
        {
            InitializeComponent();

            LayoutRoot.DataContext = this;
            _animationTimer = new DispatcherTimer(
                DispatcherPriority.ContextIdle, Dispatcher)
            { Interval = new TimeSpan(0, 0, 0, 0, 75) };
        }

        #endregion Constructor

        #region Private Methods

        private void Start()
        {
            //            Mouse.OverrideCursor = Cursors.Wait;
            _animationTimer.Tick += HandleAnimationTick;
            _animationTimer.Start();
        }

        private void Stop()
        {
            _animationTimer.Stop();
            //            Mouse.OverrideCursor = Cursors.Arrow;
            _animationTimer.Tick -= HandleAnimationTick;
        }

        private void HandleAnimationTick(object sender, EventArgs e)
        {
            SpinnerRotate.Angle = (SpinnerRotate.Angle + 16) % 360;
        }

        private void HandleLoaded(object sender, RoutedEventArgs e)
        {
            const double offset = Math.PI;
            const double step = Math.PI * 2 / 10.0;

            SetPosition(C0, offset, 0.0, step);
            SetPosition(C1, offset, 1.0, step);
            SetPosition(C2, offset, 2.0, step);
            SetPosition(C3, offset, 3.0, step);
            SetPosition(C4, offset, 4.0, step);
            SetPosition(C5, offset, 5.0, step);
            SetPosition(C6, offset, 6.0, step);
            SetPosition(C7, offset, 7.0, step);
            SetPosition(C8, offset, 8.0, step);
        }

        private void SetPosition(Ellipse ellipse, double offset,
            double posOffSet, double step)
        {
            ellipse.SetValue(Canvas.LeftProperty, 15.0
                + Math.Sin(offset + posOffSet * step) * 15.0);

            ellipse.SetValue(Canvas.TopProperty, 15
                + Math.Cos(offset + posOffSet * step) * 15.0);
        }

        private void HandleUnloaded(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        private void HandleVisibleChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            var isVisible = (bool)e.NewValue;

            if (isVisible)
                Start();
            else
                Stop();
        }

        #endregion Private Methods

        #region public Methods

        private string _title_message = "检测中...";

        /// <summary>
        /// 展示信息
        /// </summary>
        public string Title_Message
        {
            get { return _title_message; }
            set
            {
                _title_message = value;
                RaisePropertyChanged("Title_Message");
            }
        }

        #endregion public Methods

        #region INotifyPropertyChanged接口实现

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged接口实现
    }
}