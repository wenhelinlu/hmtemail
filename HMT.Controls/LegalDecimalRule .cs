﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace HMT.Controls
{
    public class LegalDecimalRule : ValidationRule
    {
        public LegalDecimalRule()
        {
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            decimal tmp = 0m;

            if (value == null || string.IsNullOrWhiteSpace(value.ToString()) || !decimal.TryParse(value.ToString(), out tmp))
            {
                return new ValidationResult(false, "请输入数值.");
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }
}