﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 根据bool值来确定加载的图标
    /// </summary>
    public class Bool2LightColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = System.Convert.ToBoolean(value);
            return b ? "/HMTERPClient.Resources;component/Resources/greenlight.png" : "/HMTERPClient.Resources;component/Resources/redlight.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}