﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    public class Bool2ImageConsecutiveNumbersConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = System.Convert.ToBoolean(value);
            return b ? "/HMTERPClient.Resources;component/Resources/Scan.png" : "/HMTERPClient.Resources;component/Resources/righttag.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}