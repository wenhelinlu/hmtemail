﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    public class CheckBoxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string type = (parameter ?? "").ToString();
            bool result = false;
            switch (type)
            {
                case "doc":  //资料
                    result = value == null ? false : value.Equals("発送完");
                    break;

                case "book": //契约书和决济书
                    result = value == null ? false : value.Equals("作成完");
                    break;

                case "confirm": //是否承认
                    result = value == null ? false : value.Equals("承認");
                    break;

                default:
                    break;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string type = (parameter ?? "").ToString();
            string result = "";
            switch (type)
            {
                case "doc":
                    result = value != null && value.Equals(true) ? "発送完" : "";
                    break;

                case "book":
                    result = value != null && value.Equals(true) ? "作成完" : "";
                    break;

                case "confirm":
                    result = value != null && value.Equals(true) ? "承認" : "";
                    break;

                default:
                    break;
            }
            return result;
        }
    }
}