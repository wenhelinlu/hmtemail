﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Timers;
using System.Windows;

namespace HMT.Controls
{
    /// <summary>
    /// UI元素焦点监视。
    /// 如果指定的元素没有获得焦点，则焦点回到默认元素
    /// </summary>
    public class UIElementFocusMonitor : IDisposable
    {
        private Timer timer = new Timer();
        private BackgroundWorker worker = new BackgroundWorker();
        private SynchronizedCollection<UIElement> elements = new SynchronizedCollection<UIElement>();
        private UIElement defaultElement;
        private UIElement onlyFocusElement;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="defaultElement"></param>
        /// <param name="interval"></param>
        public UIElementFocusMonitor(UIElement defaultElement, int interval)
        {
            this.defaultElement = defaultElement;
            this.elements.Add(defaultElement);
            this.timer.Interval = interval;
            this.timer.Enabled = true;
            this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            this.worker.DoWork += new DoWorkEventHandler(worker_DoWork);
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (onlyFocusElement != null)
                    {
                        onlyFocusElement.Focus();
                        return;
                    }

                    if (hasFocused() == false)
                    {
                        defaultElement.Focus();
                    }
                }));
            }
        }

        /// <summary>
        /// 关注的元素是否已经获得焦点
        /// </summary>
        /// <returns></returns>
        private bool hasFocused()
        {
            foreach (UIElement element in elements)
            {
                if (element.IsFocused) return true;
            }
            return false;
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (worker.IsBusy == false)
            {
                try
                {
                    worker.RunWorkerAsync();
                }
                catch (Exception ex)
                {
                }
            }
        }

        /// <summary>
        /// 开始
        /// </summary>
        public UIElementFocusMonitor Start()
        {
            this.timer.Start();
            return this;
        }

        /// <summary>
        /// 新增元素
        /// </summary>
        /// <param name="element"></param>
        public UIElementFocusMonitor Add(UIElement element)
        {
            this.elements.Add(element);
            return this;
        }

        /// <summary>
        /// 设置焦点对应的组件
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public UIElementFocusMonitor OnlyFocusWith(UIElement element)
        {
            this.onlyFocusElement = element;
            return this;
        }

        /// <summary>
        /// 禁用焦点组件
        /// </summary>
        /// <returns></returns>
        public UIElementFocusMonitor DisableOnlyFocus()
        {
            this.onlyFocusElement = null;
            return this;
        }

        /// <summary>
        /// 删除元素
        /// </summary>
        /// <param name="element"></param>
        public UIElementFocusMonitor Remove(UIElement element)
        {
            this.elements.Remove(element);
            return this;
        }

        /// <summary>
        /// 停止
        /// </summary>
        public UIElementFocusMonitor Stop()
        {
            this.timer.Stop();
            if (this.worker.IsBusy)
                this.worker.CancelAsync();
            return this;
        }

        public void Dispose()
        {
            this.Stop();
            this.elements.Clear();
            this.timer.Elapsed -= new ElapsedEventHandler(timer_Elapsed);
            this.worker.DoWork -= new DoWorkEventHandler(worker_DoWork);
            this.elements = null;
            this.timer = null;
            this.worker = null;
            this.defaultElement = null;
        }
    }
}