﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace HMT.Controls
{
    public class ConsecutiveNumbersTextBox : TextBox
    {
        public SolidColorBrush StatusImage
        {
            get { return (SolidColorBrush)GetValue(StatusImageProperty); }
            set { SetValue(StatusImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusImageProperty =
            DependencyProperty.Register("StatusImage", typeof(SolidColorBrush), typeof(ConsecutiveNumbersTextBox), new PropertyMetadata(null));
    }
}