﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace HMT.Controls
{
    public class RegexTextBoxValidationRule : ValidationRule
    {
        #region 属性

        private string _regexExp;

        /// <summary>
        /// 正则表达式
        /// </summary>
        public string RegexExp
        {
            get { return _regexExp; }
            set { _regexExp = value; }
        }

        private RegexOptions _regexOptions = RegexOptions.None;

        /// <summary>
        /// 正则表达式选项
        /// </summary>
        public RegexOptions RegexOptions
        {
            get { return _regexOptions; }
            set { _regexOptions = value; }
        }

        private string _errorMessage;

        /// <summary>
        /// 错误提示信息
        /// </summary>
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        private bool _allowNull;

        /// <summary>
        /// 是否允许为空
        /// </summary>
        public bool AllowNull
        {
            get { return _allowNull; }
            set { _allowNull = value; }
        }

        #endregion 属性

        #region 构造函数

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public RegexTextBoxValidationRule() { }

        /// <summary>
        /// 使用正则表达式创建实例
        /// </summary>
        /// <param name="regexExp">正则表达式</param>
        public RegexTextBoxValidationRule(string regexExp, bool allowNull)
        {
            this.RegexExp = regexExp;
            this.AllowNull = allowNull;
            this.ErrorMessage = "无效输入";
        }

        /// <summary>
        /// 使用正则表达式和错误提示信息创建实例
        /// </summary>
        /// <param name="regexExp">正则表达式</param>
        /// <param name="errorMsg">错误提示信息</param>
        public RegexTextBoxValidationRule(string regexExp, bool allowNull, string errorMsg)
            : this(regexExp, allowNull)
        {
            this.ErrorMessage = errorMsg;
            this.RegexOptions = _regexOptions;
        }

        /// <summary>
        /// 使用正则表达式，错误提示信息和正则表达式选项创建实例
        /// </summary>
        /// <param name="regexExp">正则表达式</param>
        /// <param name="errorMsg">错误提示信息</param>
        /// <param name="regexOptions">正则表达式选项</param>
        public RegexTextBoxValidationRule(string regexExp, bool allowNull, string errorMsg, RegexOptions regexOptions)
            : this(regexExp, allowNull, errorMsg)
        {
            this.RegexOptions = regexOptions;
        }

        #endregion 构造函数

        #region 验证方法

        /// <summary>
        /// 使用正则表达式验证输入的值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            ValidationResult result = ValidationResult.ValidResult;
            string text = value as string ?? String.Empty;

            //如果字段不允许为空，但是没有值则返回验证错误结果
            if (!AllowNull && String.IsNullOrEmpty(text))
            {
                return new ValidationResult(false, this.ErrorMessage);
            }

            //如果正则表达式不为空则根据正则表达式验证
            if (!String.IsNullOrEmpty(this.RegexExp))
            {
                if (!RegexExp.StartsWith("^"))
                {
                    RegexExp = "^" + RegexExp;
                }
                if (!RegexExp.EndsWith("$"))
                {
                    RegexExp = RegexExp + "$";
                }

                if (!Regex.IsMatch(text, this.RegexExp, this.RegexOptions))
                {
                    result = new ValidationResult(false, this.ErrorMessage);
                }
            }
            return result;
        }

        #endregion 验证方法
    }
}