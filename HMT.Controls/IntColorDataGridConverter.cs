﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace HMT.Controls
{
    public class IntColorDataGridConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int b = int.Parse(value.ToString());
            SolidColorBrush brush = null;
            //默认是未到税款所属期颜色
            System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml("#424242");

            switch (b)
            {
                case 0:
                    color = System.Drawing.ColorTranslator.FromHtml("#424242"); //黑色
                    break;

                case 1:
                    color = System.Drawing.ColorTranslator.FromHtml("#48b2ef"); //蓝色
                    break;

                case 2:
                    color = System.Drawing.ColorTranslator.FromHtml("#f8575f"); //红色
                    break;

                default:
                    color = System.Drawing.ColorTranslator.FromHtml("#424242");
                    break;
            }
            return new SolidColorBrush(Color.FromArgb(color.A, color.R, color.G, color.B)); ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}