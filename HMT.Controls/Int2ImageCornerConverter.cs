﻿using System;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    ///根据数字内容转换成对应的图片路径
    /// </summary>
    public class Int2ImageCornerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());

            string path = "";

            switch (v)
            {
                case 1:  //表示已过税款所属期状态
                    path = "/HMTERPClient.Resources;component/Resources/check.png";
                    break;

                case 2: //表示当前所属期状态
                    path = "/HMTERPClient.Resources;component/Resources/pencil.png";
                    break;

                case 3: //表示未到税款所属期状态
                    path = "/HMTERPClient.Resources;component/Resources/lockauth.png";
                    break;

                default: //其他
                    break;
            }
            return path;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}