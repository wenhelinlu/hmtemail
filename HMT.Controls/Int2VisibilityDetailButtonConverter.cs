﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HMT.Controls
{
    /// <summary>
    /// 当已过税款所属期状态显示查看明细按钮
    /// </summary>
    public class Int2VisibilityDetailButtonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());
            Visibility status = Visibility.Collapsed;
            switch (v)
            {
                case 1: //表示已过税款所属期状态
                    status = Visibility.Visible;
                    break;

                default: //其他
                    break;
            }
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}