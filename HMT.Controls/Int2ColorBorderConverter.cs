﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace HMT.Controls
{
    public class Int2ColorBorderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int v = int.Parse(value.ToString());

            //默认是未到税款所属期颜色
            System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml("#ddd");

            switch (v)
            {
                case 2: //表示当前所属期状态
                    color = System.Drawing.ColorTranslator.FromHtml("#ff6900");
                    break;
            }
            SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(color.A, color.R, color.G, color.B));
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}