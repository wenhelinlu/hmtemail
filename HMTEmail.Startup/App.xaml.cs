﻿using HMTEmail.Startup.View;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace HMTEmail.Startup
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        private System.Threading.Mutex mutex;
        private bool isCreateNew;
        //private LierdaCracker cracker = new LierdaCracker();

        protected override void OnStartup(StartupEventArgs e)
        {
            //cracker.Cracker(30);//垃圾回收间隔时间

            //log4net.Config.XmlConfigurator.Configure();//初始化log4net读取config配置，没有此句会导致日志写入无效

            DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

            try
            {
                Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
                if (!CheckNewApp()) this.Shutdown();

                WindowLogin login = new WindowLogin();
                bool? dlgResult = login.ShowDialog();
                if (dlgResult.HasValue && dlgResult.Value)
                {
                    base.OnStartup(e);
                    Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnMainWindowClose;
                }
                else
                {
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                //GlobalRunning.Logger.Error(ex);
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            if (isCreateNew)
            {
                mutex.ReleaseMutex();
            }
        }

        private bool CheckNewApp()
        {
            mutex = new System.Threading.Mutex(true, "HMTEmail.Startup", out isCreateNew);
            if (isCreateNew)
            {
                return true;
            }
            else
            {
                MessageBox.Show("程序已启动!");
                return false;
            }
        }

        /// <summary>
        /// 全局异常处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            //GlobalRunning.Logger.Error(e.Exception.Message, e.Exception);
            e.Handled = true;
        }
    }
}