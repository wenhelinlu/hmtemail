﻿using HMTEmail.Startup.Model;
using HMTEmail.Startup.ViewModel;
using LumiSoft.Net.IMAP;
using LumiSoft.Net.IMAP.Client;
using LumiSoft.Net.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Util.Controls;

namespace HMTEmail.Startup.View
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : WindowBase
    {
        public MainViewModel ViewModel
        {
            get
            {
                return this.DataContext as MainViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MainViewModel();
        }

        private void Btn_test_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.InitCommand != null)
            {
                ViewModel.InitCommand.Execute(null);
            }
        }

        private void Tv_folders_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (ViewModel.LoadEmailListCommand != null)
            {
                ViewModel.LoadEmailListCommand.Execute(((sender as TreeView).SelectedItem as NodeX).Tag);
            }
        }

        private void Lv_emaillist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView o = sender as ListView;
            string uid = (o.SelectedItem as EmailItem).UID.ToString();
            if (ViewModel.LoadMessageCommand != null)
            {
                ViewModel.LoadMessageCommand.Execute(uid);
            }
        }
    }
}