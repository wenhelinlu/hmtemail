﻿using HMTEmail.Startup.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Util.Controls;

namespace HMTEmail.Startup.View
{
    /// <summary>
    /// WindowLogin.xaml 的交互逻辑
    /// </summary>
    public partial class WindowLogin : Window
    {
        public LoginViewModel ViewModel
        {
            get
            {
                return this.DataContext as LoginViewModel;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public WindowLogin()
        {
            InitializeComponent();
            ViewModel = new LoginViewModel();
        }

        private void Ckb_setemailserver_Click(object sender, RoutedEventArgs e)
        {
            if (ckb_setemailserver.IsChecked ?? false)
            {
                this.grid_settings.Visibility = Visibility.Visible;
            }
            else
            {
                this.grid_settings.Visibility = Visibility.Collapsed;
            }
        }

        private void Btn_close_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Win_login_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.InitCommand != null)
            {
                ViewModel.InitCommand.Execute(null);
            }
        }
    }
}