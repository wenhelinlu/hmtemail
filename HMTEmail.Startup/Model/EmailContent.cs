﻿using LumiSoft.Net.Mail;
using LumiSoft.Net.MIME;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMTEmail.Startup.Model
{
    /// <summary>
    /// Email内容
    /// </summary>
    public class EmailContent
    {
        public string Text { get; set; }

        public Mail_Message Tag { get; set; }

        public List<MessageAttachments> Attachments { get; set; }

        public EmailContent()
        {
            Text = string.Empty;
            Tag = null;
            Attachments = new List<MessageAttachments>();
        }
    }

    /// <summary>
    /// 附件内容
    /// </summary>
    public class MessageAttachments
    {
        public string Text { get; set; }

        public MIME_Entity Tag { get; set; }

        public int ImageIndex { get; set; }
    }
}