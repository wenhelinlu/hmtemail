﻿using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMTEmail.Startup.Model
{
    public class NodeX : NotificationObject
    {
        #region Property

        private NodeX _parent;

        public NodeX Parent
        {
            get { return _parent; }
            set
            {
                _parent = value;
                RaisePropertyChanged(() => Parent);
            }
        }

        private int _imageIndex;

        public int ImageIndex
        {
            get { return _imageIndex; }
            set
            {
                _imageIndex = value;
                RaisePropertyChanged(() => ImageIndex);
            }
        }

        private string _Text;

        /// <summary>
        /// 显示的文本值
        /// </summary>
        public string Name
        {
            get { return this._Text; }
            set
            {
                this._Text = value;
                RaisePropertyChanged(() => Name);
            }
        }

        private string _tag;

        public string Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                _tag = value;
                RaisePropertyChanged(() => Tag);
            }
        }

        private bool? _Checked;

        /// <summary>
        /// 是否选中
        /// </summary>
        public bool? Checked
        {
            get { return this._Checked; }
            set
            {
                this.SetIsChecked(value, true, true);
                RaisePropertyChanged(() => Checked);
            }
        }

        private bool _IsExpanded;

        /// <summary>
        /// 是否展开
        /// </summary>
        public bool IsExpanded
        {
            get { return this._IsExpanded; }
            set
            {
                this._IsExpanded = value;
                RaisePropertyChanged(() => IsExpanded);
            }
        }

        /// <summary>
        /// 节点图标：相对路径
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 子节点，默认null
        /// </summary>
        public IList<NodeX> Nodes { get; set; }

        /// <summary>
        /// 该节点数据项，默认null
        /// </summary>
        public virtual IList ItemSource { get; set; }

        /// <summary>
        /// 视图控件，只有当ViewType=UserControl时才有效
        /// </summary>
        public string ViewControl { get; set; }

        #endregion Property

        #region NodeX-构造函数（初始化）

        /// <summary>
        ///  NodeX-构造函数（初始化）
        /// </summary>
        public NodeX()
        {
            this.ImageIndex = 0;
            this.Name = string.Empty;
            this.Tag = string.Empty;
            this.Checked = false;
            this.Nodes = new List<NodeX>();
        }

        public NodeX(string name) : this()
        {
            this.Name = name;
        }

        #endregion NodeX-构造函数（初始化）

        private void SetIsChecked(bool? value, bool updateChildren, bool updateParent)
        {
            if (value == _Checked)
                return;

            _Checked = value;

            if (updateChildren && _Checked.HasValue && this.Nodes != null)
                ((List<NodeX>)this.Nodes).ForEach(c => c.SetIsChecked(_Checked, true, false));

            if (updateParent && _parent != null)
                _parent.VerifyCheckState();

            RaisePropertyChanged(() => Checked);
        }

        private void VerifyCheckState()
        {
            bool? state = null;
            for (int i = 0; i < this.Nodes.Count; ++i)
            {
                bool? current = this.Nodes[i].Checked;
                if (i == 0)
                {
                    state = current;
                }
                else if (state != current)
                {
                    state = null;
                    break;
                }
            }
            this.SetIsChecked(state, false, true);
        }

        /// <summary>
        /// 设置所有子项的选中状态
        /// </summary>
        /// <param name="isChecked"></param>
        public void SetChildrenChecked(bool isChecked)
        {
            foreach (NodeX child in Nodes)
            {
                child.Checked = isChecked;
                child.SetChildrenChecked(isChecked);
            }
        }

        /// <summary>
        /// 设置所有子项展开状态
        /// </summary>
        /// <param name="isExpanded"></param>
        public void SetChildrenExpanded(bool isExpanded)
        {
            foreach (NodeX child in Nodes)
            {
                child.IsExpanded = isExpanded;
                child.SetChildrenExpanded(isExpanded);
            }
        }
    }
}