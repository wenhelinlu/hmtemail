﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMTEmail.Startup.Model
{
    /// <summary>
    /// Email Model
    /// </summary>
    public class EmailItem
    {
        /// <summary>
        /// 发件人
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// 邮件主题
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 接收时间
        /// </summary>
        public string Received { get; set; }

        /// <summary>
        /// 邮件大小
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// UID
        /// </summary>
        public long UID { get; set; }
    }
}