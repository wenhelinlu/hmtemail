﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMTEmail.Startup.Model
{
    /// <summary>
    /// 邮件安全协议
    /// </summary>
    public enum EmailSecurityProtocal
    {
        NO,
        TLS,
        SSL
    }
}