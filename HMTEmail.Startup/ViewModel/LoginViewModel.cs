﻿using HMTEmail.Runtime;
using HMTEmail.Startup.Model;
using LumiSoft.Net.IMAP.Client;
using LumiSoft.Net.Log;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace HMTEmail.Startup.ViewModel
{
    public class LoginViewModel : NotificationObject
    {
        #region Fields & Properties

        private EventHandler<WriteLogEventArgs> m_pLogCallback = null;

        private string _userName;

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        private string _password;

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        private EmailSecurityProtocal _securityProtocal = EmailSecurityProtocal.SSL;

        /// <summary>
        /// 邮件安全协议
        /// </summary>
        public EmailSecurityProtocal SecurityProtocal
        {
            get
            {
                return _securityProtocal;
            }
            set
            {
                _securityProtocal = value;
                RaisePropertyChanged(() => SecurityProtocal);
            }
        }

        private string _emailServer;

        public string EmailServer
        {
            get
            {
                return _emailServer;
            }
            set
            {
                _emailServer = value;
                RaisePropertyChanged(() => EmailServer);
            }
        }

        private string _serverPort;

        public string ServerPort
        {
            get
            {
                return _serverPort;
            }
            set
            {
                _serverPort = value;
                RaisePropertyChanged(() => ServerPort);
            }
        }

        #endregion Fields & Properties

        #region Commands

        public ICommand InitCommand { get; set; }

        public ICommand LoginCommand { get; set; }

        #endregion Commands

        public LoginViewModel()
        {
            InitCommand = new DelegateCommand(Init);
            LoginCommand = new DelegateCommand<object>(Login);
        }

        #region Methods

        private void Init()
        {
            EmailServer = "imap.naver.com";
            UserName = "hmt_2018";
            Password = "18602105249hmt";
        }

        private void Login(object sender)
        {
            if (CheckData())
            {
                IMAP_Client imap = new IMAP_Client();
                try
                {
                    imap.Logger = new Logger();
                    imap.Logger.WriteLog += m_pLogCallback;

                    imap.Connect(EmailServer, GetServerPort(), SecurityProtocal == EmailSecurityProtocal.SSL);
                    if (SecurityProtocal == EmailSecurityProtocal.TLS)
                    {
                        imap.StartTls();
                    }
                    imap.Login(UserName, Password);

                    GlobalRunning.CurrentIMAPClient = imap;
                    (sender as Window).DialogResult = true;
                }
                catch (Exception x)
                {
                    imap.Dispose();
                }
            }
        }

        /// <summary>
        /// 获取邮件服务器端口号
        /// </summary>
        /// <returns></returns>
        private int GetServerPort()
        {
            int port = 143;
            switch (SecurityProtocal)
            {
                case EmailSecurityProtocal.SSL:
                    port = 993;
                    break;

                default:
                    port = 143;
                    break;
            }
            return port;
        }

        private bool CheckData()
        {
            if (string.IsNullOrWhiteSpace(EmailServer))
            {
                MessageBoxX.Warning("Please Set Email Server.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(UserName))
            {
                MessageBoxX.Warning("Please Input UserName.");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                MessageBoxX.Warning("Please Input Password.");
                return false;
            }

            return true;
        }

        #endregion Methods
    }

    /**
            *
        IMAP服务器名称：imap.naver.com
        SMTP服务器名称：smtp.naver.com
        IMAP端口：993，需要安全连接（SSL）
        SMTP端口：587，需要安全连接（TLS）（如果没有TLS，则为SSL连接）
        用户名： hmt_2018
        密码：Naver登录密码

        hmt_2018@naver.com（18602105249hmt）

        **/
}