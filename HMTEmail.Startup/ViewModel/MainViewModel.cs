﻿using HMTEmail.Runtime;
using HMTEmail.Startup.Model;
using LumiSoft.Net;
using LumiSoft.Net.IMAP;
using LumiSoft.Net.IMAP.Client;
using LumiSoft.Net.Mail;
using LumiSoft.Net.MIME;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HMTEmail.Startup.ViewModel
{
    public class MainViewModel : NotificationObject
    {
        #region Fields & Properties

        /// <summary>
        /// IMAP_Client实例
        /// </summary>
        private IMAP_Client m_pImap = GlobalRunning.CurrentIMAPClient;

        private List<NodeX> _nodes = new List<NodeX>();

        /// <summary>
        /// 权限树节点
        /// </summary>
        public List<NodeX> Nodes
        {
            get
            {
                return _nodes;
            }
            set
            {
                _nodes = value;
                RaisePropertyChanged(() => Nodes);
            }
        }

        private List<EmailItem> _emailList = new List<EmailItem>();

        public List<EmailItem> EmailList
        {
            get
            {
                return _emailList;
            }
            set
            {
                _emailList = value;
                RaisePropertyChanged(() => EmailList);
            }
        }

        private List<EmailItem> _emails = new List<EmailItem>();

        private EmailContent _email = new EmailContent();

        /// <summary>
        /// 邮件内容
        /// </summary>
        public EmailContent Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                RaisePropertyChanged(() => Email);
            }
        }

        #endregion Fields & Properties

        #region Commands

        public ICommand InitCommand { get; set; }

        public ICommand LoadEmailListCommand { get; set; }

        public ICommand LoadMessageCommand { get; set; }

        #endregion Commands

        public MainViewModel()
        {
            InitCommand = new DelegateCommand<object>(Init);
            LoadEmailListCommand = new DelegateCommand<string>(LoadEmailList);
            LoadMessageCommand = new DelegateCommand<string>(LoadMessage);
        }

        #region Methods

        private void Init(object sender)
        {
            LoadFolders();
        }

        #region LoadFolders

        private NodeX FindNode(IList<NodeX> nodes, string nodeName)
        {
            if (nodes != null)
            {
                foreach (NodeX node in nodes)
                {
                    if (node.Name == nodeName)
                    {
                        return node;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Loads IMAP server folders to UI.
        /// </summary>
        private void LoadFolders()
        {
            List<NodeX> tmp = new List<NodeX>();
            NodeX nodeMain = new NodeX("IMAP folders");
            nodeMain.Tag = "";

            try
            {
                IMAP_r_u_List[] folders = m_pImap.GetFolders(null);

                char folderSeparator = m_pImap.FolderSeparator;
                foreach (IMAP_r_u_List folder in folders)
                {
                    string[] folderPath = folder.FolderName.Split(folderSeparator);

                    // Conatins sub folders.
                    if (folderPath.Length > 1)
                    {
                        IList<NodeX> nodes = nodeMain.Nodes;
                        string currentPath = "";

                        foreach (string fold in folderPath)
                        {
                            if (currentPath.Length > 0)
                            {
                                currentPath += "/" + fold;
                            }
                            else
                            {
                                currentPath = fold;
                            }

                            NodeX node = FindNode(nodes, fold);
                            if (node == null)
                            {
                                node = new NodeX(fold);
                                node.Tag = currentPath;
                                nodes.Add(node);
                            }

                            nodes = node.Nodes;
                        }
                    }
                    else
                    {
                        NodeX node = new NodeX(folder.FolderName);
                        node.ImageIndex = 0;
                        node.Tag = folder.FolderName;
                        nodeMain.Nodes.Add(node);
                    }
                }

                tmp.Add(nodeMain);
                Nodes = tmp;
            }
            catch (Exception ex)
            {
                GlobalRunning.Logger.Error(ex);
            }
        }

        #endregion LoadFolders

        #region method LoadFolderMessages

        /// <summary>
        /// Gets specified folder messages list from IMAP server and adds them to UI.
        /// </summary>
        /// <param name="folder">IMAP folder which messages to load.</param>
        private void LoadEmailList(string folder)
        {
            try
            {
                EmailList.Clear();
                _emails.Clear();

                m_pImap.SelectFolder(folder);

                // No messages in folder, skip fetch.
                if (m_pImap.SelectedFolder.MessagesCount == 0)
                {
                    return;
                }

                // Start fetching.
                m_pImap.Fetch(
                    false,
                    IMAP_t_SeqSet.Parse("1:*"),
                    new IMAP_t_Fetch_i[]{
                        new IMAP_t_Fetch_i_Envelope(),
                        new IMAP_t_Fetch_i_Flags(),
                        new IMAP_t_Fetch_i_InternalDate(),
                        new IMAP_t_Fetch_i_Rfc822Size(),
                        new IMAP_t_Fetch_i_Uid()
                    },
                    this.m_pImap_Fetch_MessageItems_UntaggedResponse
                );

                EmailList = _emails;
            }
            catch (Exception ex)
            {
                GlobalRunning.Logger.Error(ex);
                MessageBoxX.Error(ex.Message);
            }
        }

        #endregion method LoadFolderMessages

        #region method m_pImap_Fetch_MessageItems_UntaggedResponse

        /// <summary>
        /// This method is called when FETCH (Envelope Flags InternalDate Rfc822Size Uid) untagged response is received.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void m_pImap_Fetch_MessageItems_UntaggedResponse(object sender, EventArgs<IMAP_r_u> e)
        {
            /* NOTE: All IMAP untagged responses may be raised from thread pool thread,
                so all UI operations must use Invoke.

               There may be other untagged responses than FETCH, because IMAP server
               may send any untagged response to any command.
            */

            try
            {
                if (e.Value is IMAP_r_u_Fetch)
                {
                    IMAP_r_u_Fetch fetchResp = (IMAP_r_u_Fetch)e.Value;

                    EmailItem item = new EmailItem();

                    item.UID = fetchResp.UID.UID;

                    string from = "";
                    if (fetchResp.Envelope.From != null)
                    {
                        for (int i = 0; i < fetchResp.Envelope.From.Length; i++)
                        {
                            // Don't add ; for last item
                            if (i == fetchResp.Envelope.From.Length - 1)
                            {
                                from += fetchResp.Envelope.From[i].ToString();
                            }
                            else
                            {
                                from += fetchResp.Envelope.From[i].ToString() + ";";
                            }
                        }
                    }
                    else
                    {
                        from = "<none>";
                    }
                    item.From = from;

                    item.Subject = fetchResp.Envelope.Subject != null ? fetchResp.Envelope.Subject : "<none>";

                    item.Received = fetchResp.InternalDate.Date.ToString("yyyy-MM-dd HH:mm");

                    item.Size = ((decimal)(fetchResp.Rfc822Size.Size / (decimal)1000)).ToString("f2") + " kb";

                    _emails.Add(item);
                }
            }
            catch (Exception ex)
            {
                GlobalRunning.Logger.Error(ex);
                MessageBoxX.Error(ex.Message);
            }
        }

        #endregion method m_pImap_Fetch_MessageItems_UntaggedResponse

        #region method LoadMessage

        /// <summary>
        /// Load specified IMAP message to UI.
        /// </summary>
        /// <param name="uid">Message IMAP UID value.</param>
        private void LoadMessage(string uid)
        {
            //m_pTabPageMail_MessageAttachments.Items.Clear();

            try
            {
                // Start fetching.
                m_pImap.Fetch(
                    true,
                    IMAP_t_SeqSet.Parse(uid),
                    new IMAP_t_Fetch_i[]{
                        new IMAP_t_Fetch_i_Rfc822()
                    },
                    this.m_pImap_Fetch_Message_UntaggedResponse
                );
            }
            catch (Exception ex)
            {
                GlobalRunning.Logger.Error(ex);
            }
        }

        #endregion method LoadMessage

        #region method m_pImap_Fetch_Message_UntaggedResponse

        /// <summary>
        /// This method is called when FETCH RFC822 untagged response is received.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void m_pImap_Fetch_Message_UntaggedResponse(object sender, EventArgs<IMAP_r_u> e)
        {
            /* NOTE: All IMAP untagged responses may be raised from thread pool thread,
                so all UI operations must use Invoke.

               There may be other untagged responses than FETCH, because IMAP server
               may send any untagged response to any command.
            */

            try
            {
                if (e.Value is IMAP_r_u_Fetch)
                {
                    IMAP_r_u_Fetch fetchResp = (IMAP_r_u_Fetch)e.Value;

                    fetchResp.Rfc822.Stream.Position = 0;
                    Mail_Message mime = Mail_Message.ParseFromStream(fetchResp.Rfc822.Stream);
                    fetchResp.Rfc822.Stream.Dispose();

                    EmailContent emailContent = new EmailContent();
                    emailContent.Tag = mime;
                    //m_pTabPageMail_MessageAttachments.Tag = mime;
                    foreach (MIME_Entity entity in mime.Attachments)
                    {
                        ListViewItem item = new ListViewItem();
                        MessageAttachments attachments = new MessageAttachments();
                        if (entity.ContentDisposition != null && entity.ContentDisposition.Param_FileName != null)
                        {
                            attachments.Text = entity.ContentDisposition.Param_FileName;
                        }
                        else
                        {
                            attachments.Text = "untitled";
                        }
                        attachments.ImageIndex = 0;
                        attachments.Tag = entity;

                        emailContent.Attachments.Add(attachments);
                        //m_pTabPageMail_MessageAttachments.Items.Add(item);
                    }

                    if (mime.BodyText != null)
                    {
                        emailContent.Text = mime.BodyText;
                    }

                    Email = emailContent;
                }
            }
            catch (Exception ex)
            {
                GlobalRunning.Logger.Error(ex);
            }
        }

        #endregion method m_pImap_Fetch_Message_UntaggedResponse

        #endregion Methods
    }
}